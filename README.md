# JSON Stream Logger
Simple/Efficient JSON Logger that writes to streams

## Installation

```bash
$ npm install @parthar/json-stream-logger --save
```

## Usage

```js
// create instance
var JsonStreamLogger = require("@parthar/json-stream-logger");
var logger = new JsonStreamLogger({
    meta: function () { return Object; }, // meta-data included in every log; defaults: host, pid, time
    level: "debug", // one of the SYSLOG levels; default: "info"
    streams: [] // array of writable streams; default: new-line terminated JSON.stringify'ed output to stdout
});

// logger settings
logger.setlevel("info"); // set log-level; every log less-than-or-equal to set level will be logged
logger.setStreams([...]); // set the log streams

// logging methods
logger.emergency(...); // log emergency message
logger.alert(...); // log alert message
logger.critical(...); // log critical message
logger.error(...); // log error message
logger.warning(...); // log warning message
logger.notice(...); // log notice message
logger.info(...); // log info message
logger.debug(...); // log debug message
```

## Log Levels
Based on Syslog levels as defined in RFC5424
* 0 : EMERGENCY - system is unusable
* 1 : ALERT - action must be taken immediately
* 2 : CRITICAL - system is in critical condition
* 3 : ERROR - error condition
* 4 : WARNING - warning condition
* 5 : NOTICE - normal but significant condition
* 6 : INFO - purely informational message
* 7 : DEBUG - messages to debug an application

## Log Message Format
Uses [utils.format](https://nodejs.org/api/util.html#util_util_format_format_args) to format arguments, except the last argument which is interpreted as a JSON-object if/when present.

```js
logger.emergency("hello world");
//{"host":"myhost","pid":999,"time":"2018-05-17T14:47:03.456Z","level":"emergency","msg":"hello world"}
logger.alert({"ying": "yang"});
//{"host":"myhost","pid":999,"time":"2018-05-17T14:47:03.457Z","level":"alert","ying":"yang"}
logger.critical("hello %s", "world");
//{"host":"myhost","pid":999,"time":"2018-05-17T14:47:03.458Z","level":"critical","msg":"hello world"}
logger.error("hello %s", "world", "and", "more", {"ying": "yang"});
//{"host":"myhost","pid":999,"time":"2018-05-17T14:47:03.458Z","level":"error","msg":"hello world and more","ying":"yang"}
logger.warning("hello %s %j", "world", {"ping": "pong"});
//{"host":"myhost","pid":999,"time":"2018-05-17T14:47:03.459Z","level":"warning","msg":"hello world {\"ping\":\"pong\"}"}
logger.notice("hello %s %j", "world", {"ping": "pong"}, {"ying": "yang"});
//{"host":"myhost","pid":999,"time":"2018-05-17T14:47:03.459Z","level":"notice","msg":"hello world {\"ping\":\"pong\"}","ying":"yang"}
logger.info("hello %s %% escaped", "world", {"ying": "yang"});
//{"host":"myhost","pid":999,"time":"2018-05-17T14:47:03.459Z","level":"info","msg":"hello world % escaped","ying":"yang"}
logger.debug("hello %s %% escaped %j", "world", {"ping": "pong"}, {"ying": "yang"});
//{"host":"myhost","pid":999,"time":"2018-05-17T14:49:25.823Z","level":"debug","msg":"hello world % escaped {\"ping\":\"pong\"}","ying":"yang"}
```

## Plugins
To build a plugin, simply extend stream.Writable class and implement the write-interface. These plugins can be set as logger-streams.
