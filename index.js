"use strict";

const os = require("os");
const util = require("util");
const stream = require("stream");

const UTIL_FORMAT_TOKENS = /%[sdifjoO%]/g;
const UTIL_FORMAT_ESCAPE = /%%/g;

const LOG_LEVELS = {
    "emergency": 0,
    "alert": 1,
    "critical": 2,
    "error": 3,
    "warning": 4,
    "notice": 5,
    "info": 6,
    "debug": 7
};

class ConsoleStream extends stream.Writable {
    write(json) {
        process.stdout.write(JSON.stringify(json) + os.EOL);

        return true;
    }
}

const defaults = {
    "level": "info",
    "streams": [new ConsoleStream()],
    "meta": function meta() {
        return {
            "host": os.hostname(),
            "pid": process.pid,
            "time": new Date().toISOString()
        };
    }
};

function log(that, level, ...args) {
    let msg;
    let data;
    let tokens;
    let escapes;

    if (LOG_LEVELS[level] > that._level || !args.length) {
        return;
    }
    /* eslint-disable no-magic-numbers */
    tokens = 0;
    if (args[0].match) { // is a String
        tokens = args[0].match(UTIL_FORMAT_TOKENS);
        tokens = tokens ? tokens.length : 0;
        if (tokens) {
            escapes = args[0].match(UTIL_FORMAT_ESCAPE);
            escapes = escapes ? escapes.length : 0;
            tokens -= escapes;
        }
    }
    if ((args.length === 1 || args.length > tokens + 1) &&
        typeof args[args.length - 1] === "object") {
        data = args.pop();
    }
    /* eslint-enable no-magic-numbers */
    msg = util.format.apply(null, args);
    data = Object.assign({}, that._meta(), {
        "level": level,
        "msg": msg ? msg : undefined
    }, data);
    that._streams.forEach(function eachStream(st) {
        st.write(data);
    });
}

function getLogArgs(that, level, args) {
    let arr = Array.from(args);

    arr.unshift(that, level);

    return arr;
}


class JsonStreamLogger {
    constructor(options) {
        let opts = Object.assign({}, defaults, options);

        opts.level = opts.level.toLowerCase();
        this._meta = typeof opts.meta === "function" ? opts.meta : defaults.meta;
        this.setLevel(opts.level);
        this.setStreams(opts.streams);
    }

    setLevel(level) {
        let lvl = level ? level.toLowerCase() : "";

        this._level = LOG_LEVELS[lvl] ? LOG_LEVELS[lvl] : defaults.level;
    }

    setStreams(streams) {
        this._streams = Array.isArray(streams) ? streams : [process.stdout];
    }

    emergency() {
        log.apply(null, getLogArgs(this, "emergency", arguments));
    }

    alert() {
        log.apply(null, getLogArgs(this, "alert", arguments));
    }

    critical() {
        log.apply(null, getLogArgs(this, "critical", arguments));
    }

    error() {
        log.apply(null, getLogArgs(this, "error", arguments));
    }

    warning() {
        log.apply(null, getLogArgs(this, "warning", arguments));
    }

    notice() {
        log.apply(null, getLogArgs(this, "notice", arguments));
    }

    info() {
        log.apply(null, getLogArgs(this, "info", arguments));
    }

    debug() {
        log.apply(null, getLogArgs(this, "debug", arguments));
    }
}

module.exports = JsonStreamLogger;
