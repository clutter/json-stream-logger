/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const EventEmitter = require("events");
const stream = require("stream");
const JsonStreamLogger = require("../");

const emitter = new EventEmitter();
var logger;

class TestStream extends stream.Writable {
    write(json) {
        emitter.emit("log", json.level, json);

        return true;
    }
}

function assertCommonFormat(data) {
    assert.ok(data.host);
    assert.ok(data.pid);
    assert.ok(/^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/.test(data.time));
}

before(function (done) {
    logger = new JsonStreamLogger({
        "level": "debug",
        "streams": [new TestStream()]
    });
    done();
});

describe("JsonStreamLogger", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof JsonStreamLogger, "function");
        done();
    });
    it("should log simple string", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "hello world");
            assert.strictEqual(data.level, "emergency");
            done();
        });
        logger.emergency("hello world");
    });
    it("should log simple JSON-object", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, undefined);
            assert.strictEqual(data.ying, "yang");
            assert.strictEqual(data.level, "alert");
            done();
        });
        logger.alert({
            "ying": "yang"
        });
    });
    it("should support util.format", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "hello world");
            assert.strictEqual(data.level, "critical");
            done();
        });
        logger.critical("hello %s", "world");
    });
    it("should support util.format with additional parameters", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "hello world and more");
            assert.strictEqual(data.ying, "yang");
            assert.strictEqual(data.level, "error");
            done();
        });
        logger.error("hello %s", "world", "and", "more", {
            "ying": "yang"
        });
    });
    it("should support util.format with json-formatting %j", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "hello world {\"ping\":\"pong\"}");
            assert.strictEqual(data.level, "warning");
            done();
        });
        logger.warning("hello %s %j", "world", {
            "ping": "pong"
        });
    });
    it("should support util.format with json-formatting %j and log-data", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "hello world {\"ping\":\"pong\"}");
            assert.strictEqual(data.ying, "yang");
            assert.strictEqual(data.level, "notice");
            done();
        });
        logger.notice("hello %s %j", "world", {
            "ping": "pong"
        }, {
            "ying": "yang"
        });
    });
    it("should support escaping %", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "hello world % escaped");
            assert.strictEqual(data.ying, "yang");
            assert.strictEqual(data.level, "info");
            done();
        });
        logger.info("hello %s %% escaped", "world", {
            "ying": "yang"
        });
    });
    it("should support escaping % with json-formatting %j and log-data", function (done) {
        emitter.once("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "hello world % escaped {\"ping\":\"pong\"}");
            assert.strictEqual(data.ying, "yang");
            assert.strictEqual(data.level, "debug");
            done();
        });
        logger.debug("hello %s %% escaped %j", "world", {
            "ping": "pong"
        }, {
            "ying": "yang"
        });
    });
    it("should log below set level", function (done) {
        logger = new JsonStreamLogger({
            "level": "info",
            "streams": [new TestStream()]
        });
        emitter.on("log", function (level, data) {
            assertCommonFormat(data);
            assert.strictEqual(data.msg, "should log this info message");
            assert.strictEqual(data.level, "info");
            done();
        });
        logger.debug("should not log this debug message");
        logger.info("should log this info message");
    });
});

after(function (done) {
    logger = null;
    done();
});
